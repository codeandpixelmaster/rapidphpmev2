<?php
/**
 * CacheInterface.php
 *
 * @version 1.0
 * @date 2/27/17 1:20 PM
 * @package rapidPHPCore
 */

namespace Rapid;

interface CacheInterface {

	/**
	 * @param string $key
	 * @param mixed $value
	 * @param int $expiry
	 * @return mixed
	 */
	public function set( $key, $value, $expiry );

	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get( $key );

	/**
	 * @param string $key
	 * @return bool
	 */
	public function delete( $key );

	/**
	 * @param string $key
	 * @return bool
	 */
	public function has( $key );

	/**
	 * @param $key
	 * @return array
	 */
	public function getInfo( $key );

}