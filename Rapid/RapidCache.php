<?php
/**
 * RapidCache.php
 *
 * @version 1.0
 * @date 2/27/17 1:21 PM
 * @package rapidPHPCore
 */

namespace Rapid;


class RapidCache
{

	/**
	 * @type CacheInterface
	 */
	protected $driver;


	/**
	 * RapidCache constructor.
	 *
	 * @param CacheInterface $driver
	 */
	public function __construct( CacheInterface $driver )
	{
		$this->driver = $driver;
	}


	/**
	 * @param string $key
	 * @param mixed $value
	 * @param int $expiry
	 * @return mixed
	 */
	public function set( $key, $value, $expiry )
	{
		return $this->driver->set( $key, $value, $expiry );
	}


	/**
	 * @param string $key
	 * @return mixed
	 */
	public function get( $key )
	{
		return $this->driver->get( $key );
	}


	/**
	 * @param string $key
	 * @return bool
	 */
	public function delete( $key )
	{
		return $this->driver->delete( $key );
	}


	/**
	 * @param string $key
	 * @return bool
	 */
	public function has( $key )
	{
		return $this->driver->has( $key );
	}


	/**
	 * @param $key
	 * @return array
	 */
	public function getInfo( $key )
	{
		return $this->driver->getInfo( $key );
	}
}