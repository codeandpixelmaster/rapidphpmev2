<?php
/**
 * App.php
 * Main initialization file for RapidPHP Framework
 * Gets the entire party started by just calling $app = new App();
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

namespace Rapid;

use Rapid\Helpers\Timer;

class App {

	/**
	 * @var Environment
	 */
    public $environment;

	/**
	 * @var ConfigLoader
	 */
    public $config;

	/**
	 * @var Sessions
	 */
    public $session;

	/**
	 * @var Loader
	 */
    public $loader;

	/**
	 * @var Models
	 */
	public $models;

	/**
	 * @var mixed
	 */
    public $query;

	/**
	 * @var mixed
	 */
    public $functions;

	/**
	 * @var Control
	 */
    public $controllers;

	/**
	 * @var Helper
	 */
    public $helpers;

    /**
     * App constructor.
     */
    public function __construct()
    {
        //Start the timer
        Timer::start( 'App Started' );
        
        //Set up the environment
        $this->environment = new Environment();

        //Load all the config
        $this->config = new ConfigLoader();
        
        //Start the sessions
        $this->session = Sessions::init();
        
        //Set up the helper autoloader
        $this->loader = new Loader();
        
        //Set up the models
        $this->models = new Models();
        
        //Set up the controllers
        $this->controllers = new Control();
        
        //Set up the rapid-helpers
        $this->helpers = new Helper();
        
        //Show the timer reports if specified in application/config/config.php (or environmental config.php files)
        Timer::show_report();
        Timer::reset();
    }
    //end construct

}
//end class App