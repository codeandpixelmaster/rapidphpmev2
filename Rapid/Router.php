<?php
/**
 * Router.php
 *
 * @version 1.0
 * @date 3/31/15 11:39 PM
 * @package rapidCore
 */

namespace Rapid;

class Router {

	/**
	 * @type array
	 */
	public static $routes = array();


	/**
	 * @param array $params
	 */
	public static function set_group( $params = array() )
	{
		foreach( $params as $key => $data )
		{
			self::set( $key, $data );
		}
	}


	/**
	 * @param $controller
	 * @param array $parameters
	 */
	public static function set( $controller, $parameters = array() )
	{
		self::$routes[$controller] = (array)$parameters;
	}


	/**
	 * @return array
	 */
	public static function get()
	{
		return self::$routes;
	}
}
//end Router