<?php
/**
 * autoload-references.php
 * Provides reference list for autoloader to load common/core scripts by name only
 * from /application/config/autoload.php
 * This is crosschecked by ./autoloader.php
 *
 * @version 2.0
 * @date 11-Jul-2014
 * @package RapidPHPMe
 **/    

return array(

    'forms' =>      RAPID_FUNCTIONS . 'form-helper.php', 
    'files' =>      RAPID_FUNCTIONS . 'files-helper.php', 
    'email' =>      PLUGINS . 'phpmailer-config.php', 
    'html' =>       RAPID_FUNCTIONS . 'html-helper.php', 
    'url' =>        RAPID_FUNCTIONS . 'url-helper.php', 
    'urls' =>       RAPID_FUNCTIONS . 'url-helper.php', 
    'users' =>      PLUGINS . 'users-setup.php', 
    'cache' =>      PLUGINS . 'fastcache-config.php', 
    'slug' =>       RAPID_FUNCTIONS . 'url-slug-helper.php', 
    'sendgrid' =>   PLUGINS . 'sendgrid-config.php', 

);

/* End of file autoload-references.php */
/* Location: /application/core/autoload-references.php */