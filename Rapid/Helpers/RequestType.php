<?php
/**
 * RequestType.php
 *
 * @version 1.0
 * @date 2/27/17 2:20 PM
 * @package rapidPHPCore
 */

namespace Rapid\Helpers;


class RequestType
{
	/**
	 * @type string
	 */
	public static $post = 'POST';

	/**
	 * @type string
	 */
	public static $get = 'GET';

	/**
	 * @type string
	 */
	public static $put = 'PUT';

	/**
	 * @type string
	 */
	public static $delete  = 'DELETE';

	/**
	 * @type string
	 */
	public static $head = 'HEAD';

	/**
	 * @type string
	 */
	public static $options = 'OPTIONS';


	/**
	 * @return mixed
	 */
	public static function getType()
	{
		return filter_input( INPUT_SERVER, 'REQUEST_METHOD', FILTER_SANITIZE_ENCODED );
	}


	/**
	 * @param $type
	 * @return bool
	 */
	public static function isType( $type )
	{
		return ( self::getType() == self::${$type} ) ? true : false;
	}


	/**
	 * @return bool
	 */
	public static function isInputType()
	{
		$input_types = [
			self::$post,
			self::$put,
			self::$delete,
		];
		return ( in_array( self::getType(), $input_types ) ) ? true : false;
	}

}