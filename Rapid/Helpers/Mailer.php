<?php
/**
 * Mailer.php
 *
 * @version 1.0
 * @date 2/17/17 5:17 PM
 * @package rapidphpme
 */

namespace Rapid\Helpers;


use Rapid\Options;

class Mailer extends \PHPMailer {

	/**
	 * @return \PHPMailer
	 * @throws \Exception
	 */
	public static function init()
	{
		$mail = new self();

		//Just in case there were any queued up from before, kill them
		$mail->clearAddresses();

		$mail->Debugoutput = ENVIRONMENT != 'production' ? true : false;

		$config = Options::get_mail();

		$debug = [];


		//Check to see if SMTP creds have been defined
		if( !empty( $config['smtp'] ) && !empty( $config['smtp']['user'] ) && !empty( $config['smtp']['password'] ) )
		{
			$mail->IsSMTP();
			$mail->Host = $config['smtp']['host'];
			$mail->SMTPAuth = true;
			$mail->Port = $config['smtp']['port'];
			$mail->Username = $config['smtp']['user'];
			$mail->Password = $config['smtp']['password'];
		}

		return $mail;
	}

	/**
	 * Running this instead of $mail->Send() allows the outbound emails to effectively be run through test email addresses
	 *
	 * @param \PHPMailer $mail
	 * @return bool
	 * @throws \phpmailerException
	 */
	public static function SendOut( $mail )
	{

		if( ENVIRONMENT == 'development' )
		{
			//Get a list of who it "would" have been sent to...
			$would_have = $mail->getAllRecipientAddresses();
			$would_have = array_keys( $would_have );
			$mail->Body .= "\r\n\r\nEMAIL SENT FROM ".ENVIRONMENT."\r\n\r\nWould have sent to...\r\n".implode( ', ', $would_have );

			$mail->ClearAllRecipients();
			$email_list = Options::get_config( 'error_emails' );
			foreach( $email_list as $email )
			{
				$mail->addAddress( $email );
			}
		}
		if( ENVIRONMENT == 'staging' )
		{
			//Get a list of who it "would" have been sent to...
			$would_have = $mail->getAllRecipientAddresses();
			$would_have = array_keys( $would_have );
			$mail->Body .= "\r\n\r\nEMAIL SENT FROM ".ENVIRONMENT."\r\n\r\nWould have sent to...\r\n".implode( ', ', $would_have );

			$mail->clearAllRecipients();
			$email_list = Options::get_config( 'error_emails' );
			foreach( $email_list as $email )
			{
				$mail->addAddress( $email );
			}
		}
		return $mail->Send();
	}


	/**
	 * @param $val
	 * @return string
	 */
	private static function map_replacement( $val )
	{
		return '({'.$val.'})';
	}


	/**
	 * @param $data
	 * @param $contents
	 * @return mixed
	 */
	public static function parse_template( $data, $contents )
	{
		$replace = array_keys( $data );
		$replace = array_map( array( __CLASS__, 'map_replacement' ), $replace );
		$replacements = array_values( $data );
		//Replace the codetags with the message contents
		return preg_replace( $replace, $replacements, $contents );
	}
}