<?php
/**
 * template-output.php
 * Allows custom functions to be specified and output to header and footer areas
 * using head() and footer() tags similar to wordpress wp_head and wp_footer
 *
 * @version 1.5
 * @date 08-May-2013
 * @updated 31-Aug-2016
 * @package RapidPHPMe
 **/

/**
 * Arrays to store added function outputs for header and footers
 */
$mvc_head = array();
$mvc_footer = array();


/**
 * Function called within views or controllers to load additional info
 * to head() or footer() areas
 * Example usage:
 *
 * function magic_function()
 * {
 *      echo '<!--MAGIC!-->';
 * }
 * add_output( 'header', 'magic_function' );
 *
 * function to_the_footer()
 * {
 *      echo '<script>alert("I am in your footer!");</script>';
 * }
 * add_output( 'footer', 'to_the_footer' );
 *
 * add_output( 'header', function(){
 *      echo 'this is a test';
 * });
 *
 * @access public
 * @param string $area (head, footer)
 * @param string, or array of $function (must be callable)
 * @return none
 */
function add_output( $area, $functions )
{
    global $mvc_head, $mvc_footer;
    $functions = !is_array( $functions ) ? array( $functions ) : $functions;
    foreach( $functions as $f )
    {
        if( is_callable( $f ) || $f instanceof Closure )
        {
            $k = ( $f instanceof Closure ) ? time() : $f;
            switch( $area )
            {
                case 'footer':
                    $mvc_footer[$k] = $f;
                break;
                case 'header':
                default:
                    $mvc_head[$k] = $f;
                break;
            }
        }
    }

}


/**
 * Remove user specific functions from either head or footer outputs
 * Will remove user callable function from output
 * Example:
 * remove_output( 'footer', 'to_the_footer' );
 *
 * @access public
 * @param string $area
 * @param string $function name
 * @return mixed (updated arrays)
 */
function remove_output( $area, $function )
{
	global $mvc_head, $mvc_footer;

    if( $area == 'header' && in_array( $function, $mvc_head ) )
    {
        $mvc_head = array_diff( $mvc_head, array( $function ) );
    }
    
    if( $area == 'footer' && in_array( $function, $mvc_footer ) )
    {
        $mvc_footer = array_diff( $mvc_footer, array( $function ) );
    }
}


/**
 * Clear all user functions from mvc_head and mvc_footer
 *
 * @access public
 * @param none
 * @return none
 */
function empty_output()
{
    global $mvc_head, $mvc_footer;
    
    $mvc_head = array();
    $mvc_footer = array();
}


/**
 * Execute user specified functions within head() output
 *
 * @access public
 * @param none
 * @return mixed
 */
function mvc_head()
{
    global $mvc_head;

    if( !empty( $mvc_head ) )
    {
        foreach( $mvc_head as $data )
        {
            if( is_callable( $data ) )
            {
                call_user_func( $data );
            }
        }
        //Add a clean line break
        echo PHP_EOL;
    }
}


/**
 * Execute user specified functions within footer() output
 *
 * @access public
 * @param none
 * @return mixed
 */
function mvc_footer()
{
    global $mvc_footer;
    
    if( !empty( $mvc_footer ) )
    {
        foreach( $mvc_footer as $foot )
        {
            if( is_callable( $foot ) )
            {
                call_user_func( $foot );
            }
        }
        //Add a clean line break
        echo PHP_EOL;
    }
}

/* End of file template-output.php */
/* Location: application/core/template-output.php */