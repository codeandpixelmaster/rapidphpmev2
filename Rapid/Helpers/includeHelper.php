<?php
/**
 * includeHelper.php
 * Provides quick linking to known file locations using functions
 *
 * @version 1.0
 * @date 13-Oct-2012
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** list_helpers()   //provides a list of available helper files
 ** add_helper()     //include a specific helper file
 ** list_classes()   //provides a list of available class files
 ** add_class()      //include a specific class file
 ** add_plugin()     //include a specific plugin file
 **/

namespace Rapid\Helpers;

class includeHelper
{
    
    /**
     * Function to provide a list of available helper files
     * Scours the HELPERS directory as defined in index.php
     * @access public
     * @param none
     * @return array
     */
    function list_helpers()
    {
        $dh = opendir( HELPERS );
        $files = array();
        while( false !== ( $file = readdir( $dh ) ) ) 
        {
            if( ( substr( $file, -3 ) == "php" ) )
            {
                //Don't list subdirectories
                if( !is_dir( "$dir/$file" ) ) 
                {
                    $files[] = $file;
                }
            }
        }
        closedir( $dh );
        return $files;
    }


    /**
     * Function to include a specific helper file
     * Loads file from the HELPERS directory as defined in index.php
     * @access public
     * @param string $helper filename
     * @return mixed
     */
    function add_helper( $helper )
    {
        $helper = trim( $helper );   
        $file = HELPERS . $helper;
        
         //Add them both back and include the file if it exists
        if( file_exists( $file ) )
        {
            include_once( $file );
        }
        else
        {
            trigger_error( 'The file: '. $file .' was not found' );
        }
    }


    /**
     * Function to provide a list of available class files
     * Scours the CLASSES directory as defined in index.php
     * @access public
     * @param none
     * @return array
     */
    function list_classes()
    {
        $dh = opendir( CLASSES );
        $files = array();
        while( false !== ( $file = readdir( $dh ) ) ) 
        {
            if( ( substr( $file, -3 ) == "php" ) )
            {
                //Don't list subdirectories
                if( !is_dir( "$dh/$file" ) ) 
                {
                    $files[] = $file;
                }
            }
        }
        closedir( $dh );
        return $files;
    }


    /**
     * Function to include a specific class file
     * Loads file from the CLASSES directory as defined in index.php
     * @access public
     * @param string $class
     * @return mixed
     */
    function add_class( $class )
    {
        //Remove .php extension
        $class = trim( rtrim( $class, '.php' ) );
        
        $file = CLASSES . $class .'.php';
        
        //Add the file if it exists
        if( file_exists( $file ) )
        {
            include_once( CLASSES . $class .'.php' );
        }
        else
        {
            trigger_error( 'The file: '. $file .' was not found' );
        }
    }


    /**
     * Function to include a specific plugin file
     * Loads file from the PLUGINS directory as defined in index.php
     * @access public
     * @param string $plugin
     * @return mixed
     */    
    function add_plugin( $plugin )
    {
        //Remove .php extension
        $plugin = trim( $plugin );
        
        $file = PLUGINS . $plugin;
        
        //Add the file if it exists
        if( file_exists( $file ) )
        {
            require_once( $file );
        }
        else
        {
            trigger_error( 'The file: '. $file .' was not found' );
        }
    }
    
} //end class includeHelper

/* End of file includeHelper.php */
/* Location: application/core/includeHelper.php */