<?php
/**
 * EventLogger.php
 *
 * @version 1.1
 * @date 8/5/16 10:46 PM
 * @package rapidCore
 */
namespace Rapid\Helpers;

class EventLogger extends DatabaseUtility {

	const TYPE_ADD = 1;
	const TYPE_DELETE = 2;
	const TYPE_EDIT = 3;
	const TYPE_UPDATE = 4;
	const TYPE_CANCEL = 5;
	const TYPE_EMAIL = 6;
	const TYPE_LOGIN = 7;
	const TYPE_LOGOUT = 8;
	const TYPE_API = 9;
	const TYPE_REGISTER = 10;

	/**
	 * @type array
	 */
	public static $types = [
		self::TYPE_ADD => 'Added',
		self::TYPE_DELETE => 'Deleted',
		self::TYPE_EDIT => 'Edited',
		self::TYPE_UPDATE => 'Updated',
		self::TYPE_CANCEL => 'Cancelled',
		self::TYPE_EMAIL => 'Email',
		self::TYPE_LOGIN => 'Login',
		self::TYPE_LOGOUT => 'Logout',
		self::TYPE_API => 'API Request',
		self::TYPE_REGISTER => 'Registration',
	];


	/**
	 * @type string
	 */
	protected static $table = 'event_logger';

	/**
	 * @type null|int
	 */
	protected static $user = null;

	/**
	 * @type bool|EventLogger
	 */
	protected static $setup = false;

	/**
	 * @type null|SimplePDO
	 */
	protected static $database = null;


	/**
	 * EventLogger constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		self::$database = $this->db;
		if( !$this->db->table_exists( self::$table ) )
		{
			$this->setup_table();
		}

		if( null !== session( 'user_id' ) )
		{
			self::$user = session( 'user_id' );
		}
	}


	/**
	 * Generate the tables needed for this as necessary
	 */
	private function setup_table()
	{
		$this->add( self::$table )
			->column( 'id', 'int(11)', 'DEFAULT NULL', true )
			->column( 'type', 'tinyint(1) unsigned', 'DEFAULT NULL' )
			->column( 'user', 'int(11)' )
			->column( 'label', 'varchar(220)', 'DEFAULT NULL' )
			->column( 'recorded', 'timestamp', 'NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP' )
			->column( 'category', 'varchar(220)', 'DEFAULT NULL' )
			->column( 'value', 'text' )
			->run();
	}


	/**
	 * Singleton function to internally init and store
	 */
	private static function init()
	{
		if( !self::$setup )
		{
			self::$setup = new EventLogger();
		}
	}


	/**
	 * @param int $type
	 * @param null|int $user
	 * @param string $label
	 * @param null|string $category
	 * @param null|string|array|object $textstring
	 * @param bool $override If true, disables the check and update
	 * @return bool
	 */
	public static function track( $type, $user = null, $label, $category = null, $textstring = null, $override = false )
	{
		if( !in_array( $type, array_keys( self::$types ) ) )
		{
			return false;
		}

		self::init();

		if( is_null( $user ) && !is_null( self::$user ) )
		{
			$user = self::$user;
		}

		if( is_object( $label ) )
		{
			$label = json_encode( get_object_vars( $label ) );
		}
		elseif( is_array( $label  ) )
		{
			$label = json_encode( $label );
		}

		$params = [
			'type' => $type,
			'user' => $user,
			'label' => $label,
			'recorded' => right_now(),
		];

		//Find out if this log already exists, if so update it
		$exists_query = "SELECT id FROM ". self::$table . " WHERE type = ? AND label = ?";
		$exists_params = [
			$type,
			$label,
		];

		if( !is_null( $user ) )
		{
			$params['user'] = $user;
			$exists_query .= " AND user = ?";
			$exists_params[] = $user;
		}
		if( !is_null( $category ) )
		{
			$params['category'] = $category;
			$exists_query .= " AND category = ?";
			$exists_params[] = $category;
		}
		if( !is_null( $textstring ) )
		{
			$params['value'] = ( is_array( $textstring ) || is_object( $textstring ) ) ? json_encode( $textstring ) : $textstring;
			$exists_query .= " AND value = ?";
			$exists_params[] = $params['value'];
		}

		if( $override === true )
		{
			$exists = false;
		}
		else
		{
			$exists = self::$database->get_row( $exists_query .' LIMIT 1', $exists_params );
		}

		if( !empty( $exists ) && isset( $exists->id ) && !empty( $exists->id ) )
		{
			self::$database->update( self::$table, $params, array( 'id' => $exists->id ), 1 );
		}
		else
		{
			self::$database->insert( self::$table, $params );
		}
	}


	/**
	 * @param $types
	 * @param null $value
	 * @param null $category
	 * @param null $limit
	 * @param null $user
	 * @param bool|false $paginate
	 * @param bool|true $orderby Use default orderby
	 * @return array
	 */
	public static function history( $types, $value = null, $category = null, $limit = null, $user = null, $orderby = true, $paginate = false )
	{
		self::init();

		$params = [];
		$types = !is_array( $types ) ? array( $types ) : $types;

		$sql = "SELECT `type`, `label`, `recorded`, `category`, `value`, `user` FROM ". self::$table ." WHERE type IN(".self::$database->prepare_in( $types ).")";
		$params = array_merge( $params, $types );
		if( !is_null( $value ) )
		{
			$sql .= " AND label = ?";
			$params[] = $value;
		}
		if( !is_null( $category ) )
		{
			$category = !is_array( $category ) ? array( $category ) : $category;
			$sql .= " AND category IN(". self::$database->prepare_in( $category ) .")";
			$params = array_merge( $params, $category );
		}
		if( !is_null( $user ) )
		{
			$sql .= " AND user = ?";
			$params[] = $user;
		}
		if( $orderby )
		{
			$sql .= " ORDER BY recorded DESC";
		}
		if( !is_null( $limit ) )
		{
			$sql .= " LIMIT ?";
			$params[] = (int)$limit;
		}

		//If we need to paginate, return array of sql and params for users to do what they like
		if( $paginate )
		{
			return [
				'sql' => $sql,
				'params' => $params,
			];
		}

		return self::$database->get_results( $sql, $params );
	}

}