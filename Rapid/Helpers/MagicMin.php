<?php
/**
 * magic-min-config.php
 * Provides aux support to simplify magic min usage within the context of rapidphpme
 * See https://github.com/bennettstone/magic-min for more configuration options
 * Usage: MagicMin::
 *
 * @version 1.0
 * @date 10-Mar-2014
 * @package MagicMin::magic_min_merge
 **/

namespace Rapid\Helpers;

class MagicMin {

    public static $options = array(
        'echo' => false,                 //Return or echo the values
        'encode' => false,              //base64 images from CSS and include as part of the file?
        'timer' => false,                //Ouput script execution time
        'gzip' => false,                //Output as php with gzip?
        'closure' => true,              //Use google closure (utilizes cURL)
        'remove_comments' => true,      //Remove comments,
        'hashed_filenames' => true,    //Generate hashbased filenames to break caches,
        'output_log' => false,          //Output logs automatically at end of file output,
        'force_rebuild' => false,       //Brute force rebuild of minified assets- USE SPARINGLY!
    );


	/**
	 * Change any of the static options before running magic_min_merge
	 * @param string $key
	 * @param bool $value
	 */
    public static function setOption( $key, $value )
    {
        if( isset( self::$options[$key] ) )
        {
            self::$options[$key] = $value;
        }
    }
    
    /**
     * Function to simplify replacements of absolute vs. relative paths when using magicmin
     * Dependent on magicmin being included in $autoload array
     * Dependent on magicmin file existing in /rapid-helpers/ directory
     * @access public
     * @param string $output_filename
     * @param array $files
     * @param string $output_directory (just the 'css', 'js' etc... bit)
     * @param string $type (js, css)
	 * @param bool $js_async
     * @return string
     */
    public static function magic_min_merge( $output_filename, $files = array(), $output_directory = 'css', $js_async = false )
    {
        if( empty( $files ) )
        {
            return '';
        }

        $output_type = strtolower( pathinfo( basename( $output_filename ), PATHINFO_EXTENSION ) );

        $minified = new \Minifier( self::$options );

        $files = array_map( function( $path ){ return str_replace( ASSETS, ASSETS_ROOT, $path ); }, $files );
		$js_async = ( $js_async === true ) ? ' async' : '';

        switch( $output_type )
        {
            case 'css':
                return '<link rel="stylesheet" href="'. str_replace( 
                    ASSETS_ROOT, 
                    ASSETS, 
                    $minified->merge( 
                        ASSETS_ROOT . $output_directory.'/'.$output_filename, 
                        ASSETS_ROOT . $output_directory, 
                        $files, 
                        $output_type 
                    )
                ).'" />' . PHP_EOL;
            break;
            case 'js':
            default:
                return '<script'.$js_async.' src="'. str_replace(
                    ASSETS_ROOT, 
                    ASSETS, 
                    $minified->merge( 
                        ASSETS_ROOT . $output_directory.'/'.$output_filename, 
                        ASSETS_ROOT . $output_directory, 
                        $files, 
                        $output_type
                    )
                ).'"></script>' . PHP_EOL;
            break;
        }

    }
}