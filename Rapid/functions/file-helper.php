<?php
/**
 * file-helper.php
 * Provides support for zip files, mime types, etc...
 *
 * @version 1.0
 * @date 21-Apr-2014
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** include_get_contents()  	//Get and execute PHP contents from other files
 ** force_download()            //Generates headers that force a download to happen
 ** create_zip()                //Function to create zip files
 ** extract_zip()               //Extract the contents of a zip file to a directory
 ** extract_rar()               //Extract the contents of a rar file to a directory
 **/


if( !function_exists( 'include_get_contents' ) )
{
    /**
     * include_get_contents
     * include a file and return it's output
     *
     * @param string $path filename of include
     * @return string
     */
    function include_get_contents( $path )
    {
        if( file_exists( $path ) )
        {
            ob_start();
            include_once( $path );
            return ob_get_clean();
        }
    }
}


if( ! function_exists( 'force_download' ) )
{
    /**
     * Force Download
     *
     * Generates headers that force a download to happen
     * Example usage:
     * force_download( 'screenshot.png', './images/screenshot.png' );
     *
     * @access public
     * @param string $filename
     * @param string $data
     * @return bool
     */
    function force_download( $filename = '', $data = '' )
    {
        if( $filename == '' || $data == '' )
        {
            return false;
        }
        
        if( !file_exists( $data ) )
        {
            return false;
        }

        // Try to determine if the filename includes a file extension.
        // We need it in order to set the MIME type
        if( false === strpos( $filename, '.' ) )
        {
            return false;
        }
    
        // Grab the file extension
        $extension = strtolower( pathinfo( basename( $filename ), PATHINFO_EXTENSION ) );

        // our list of mime types
        $mime_types = array(

            'txt' => 'text/plain',
            'htm' => 'text/html',
            'html' => 'text/html',
            'php' => 'text/html',
            'css' => 'text/css',
            'js' => 'application/javascript',
            'json' => 'application/json',
            'xml' => 'application/xml',
            'swf' => 'application/x-shockwave-flash',
            'flv' => 'video/x-flv',

            // images
            'png' => 'image/png',
            'jpe' => 'image/jpeg',
            'jpeg' => 'image/jpeg',
            'jpg' => 'image/jpeg',
            'gif' => 'image/gif',
            'bmp' => 'image/bmp',
            'ico' => 'image/vnd.microsoft.icon',
            'tiff' => 'image/tiff',
            'tif' => 'image/tiff',
            'svg' => 'image/svg+xml',
            'svgz' => 'image/svg+xml',

            // archives
            'zip' => 'application/zip',
            'rar' => 'application/x-rar-compressed',
            'exe' => 'application/x-msdownload',
            'msi' => 'application/x-msdownload',
            'cab' => 'application/vnd.ms-cab-compressed',

            // audio/video
            'mp3' => 'audio/mpeg',
            'qt' => 'video/quicktime',
            'mov' => 'video/quicktime',

            // adobe
            'pdf' => 'application/pdf',
            'psd' => 'image/vnd.adobe.photoshop',
            'ai' => 'application/postscript',
            'eps' => 'application/postscript',
            'ps' => 'application/postscript',

            // ms office
            'doc' => 'application/msword',
            'rtf' => 'application/rtf',
            'xls' => 'application/vnd.ms-excel',
            'ppt' => 'application/vnd.ms-powerpoint',

            // open office
            'odt' => 'application/vnd.oasis.opendocument.text',
            'ods' => 'application/vnd.oasis.opendocument.spreadsheet',
        );
    
        // Set a default mime if we can't find it
        if( !isset( $mime_types[$extension] ) )
        {
            $mime = 'application/octet-stream';
        }
        else
        {
            $mime = ( is_array( $mime_types[$extension] ) ) ? $mime_types[$extension][0] : $mime_types[$extension];
        }
            
        // Generate the server headers
        if( strstr( $_SERVER['HTTP_USER_AGENT'], "MSIE" ) )
        {
            header( 'Content-Type: "'.$mime.'"' );
            header( 'Content-Disposition: attachment; filename="'.$filename.'"' );
            header( 'Expires: 0' );
            header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
            header( "Content-Transfer-Encoding: binary" );
            header( 'Pragma: public' );
            header( "Content-Length: ".filesize( $data ) );
        }
        else
        {
            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: private", false );
            header( "Content-Type: ".$mime, true, 200 );
            header( 'Content-Length: '.filesize( $data ) );
            header( 'Content-Disposition: attachment; filename='.$filename);
            header( "Content-Transfer-Encoding: binary" );
        }
        readfile( $data );
        exit;
    
    } //End force_download
}


if( !function_exists( 'create_zip' ) )
{
	/**
	 * Function to create zip files
	 * Forces download
	 * Will retrieve all files in a directory or only specified file types
	 * Usage: echo create_zip( 44 );
	 *
	 * @access public
	 * @param string $directory
	 * @param bool $all disregard file type selection
	 * @param array $list to specify list of contents
	 * @return mixed (err, forced download)
	 */
    function create_zip( $directory, $filename, $all = false, $list = array() )
    {

        //Make sure the class exists, if not err
        if( !class_exists( 'ZipArchive' ) )
        {
            return 'Class ZipArchive does not exist';
        }

        //Allowed file types used for glob selection
        $allowed = array( 'gif', 'jpg', 'png', 'mov', 'mp4', 'tiff', 'jpeg' );

        //Zipfile name
        $zip_file = $directory .'.zip';

        //Start the zip
        $zip = new ZipArchive();

        //Err if you can't open the file
        if( !$zip->open( $zip_file, ZIPARCHIVE::OVERWRITE ) )
        {
            return "Failed to create archive";
        }

        //Add every file regardless of type
        if( $all )
        {
            $zip->addGlob( $directory .'/*' );
        }
        elseif( !empty( $list ) && is_array( $list ) )
        {
            $use_files = array();
            foreach( $list as $place => $file )
            {
                if( file_exists( $file ) )
                {
                    $use_files[$file] = is_string( $place) ? $place : $file;
                }
            }
            if( !empty( $use_files ) )
            {
                foreach( $use_files as $file => $place )
                {
                    $zip->addFile( $file, $place );
                }
            }
        }
        //Add only specific file types
        else
        {
            $files = glob( $directory .'/*.{'.implode(',', $allowed ).'}', GLOB_BRACE);
            if( empty( $files ) )
            {
                return 'No matched file types located';
            }

            foreach( $files as $file )
            {
                $zip->addFile( $file );
            }
        }

        //Err if the zip wasn't able to be created for some reason
        if( !$zip->status == ZIPARCHIVE::ER_OK )
        {
            return "Failed to write files to zip";
        }

        //Close the zip
        $zip->close();

        //then send the headers to foce download the zip file
        header( "Content-type: application/zip" ); 
        header( "Content-Disposition: attachment; filename=$filename" ); 
        header( "Pragma: no-cache" );
        header( "Expires: 0" );
        readfile( "$zip_file" );

        //Delete the zip from the server
        unlink( $zip_file );
        exit;
    }
}


if( !function_exists( 'extract_zip' ) )
{
	/**
	 * Extract the contents of a zip file to a directory
	 * Example:
	 * $zipfile = dirname( __FILE__ ) .'/FreeBird - Twisted Journeys.zip';
	 * extract_zip( $zipfile, './' );
	 *
	 * @access public
	 * @param string $zip_file to extract
	 * @param string $extract_to directory
	 * @param bool $all (default false, true will unzip all files without filetype regard)
	 * @return string
	 */
    function extract_zip( $zip_file, $extract_to, $all = false )
    {
        //Make sure the class exists, if not err
        if( !class_exists( 'ZipArchive' ) )
        {
            return 'Class ZipArchive does not exist';
        }

        //Allowed file types used for glob selection
        $allowed = array( 'gif', 'jpg', 'png', 'mov', 'mp4', 'tiff', 'jpeg' );

        $zip = new ZipArchive;
        if( $zip->open( $zip_file ) )
        {
            $file_list = array();
            for( $i=0; $i < $zip->numFiles; $i++ )
            {
                $filename = $zip->getNameIndex( $i );
                $full_name = $zip->statIndex( $i );
                if( !( $full_name['name'][strlen( $full_name['name'] )-1] == "/" ) )
                {
                    if( preg_match( '#\.('.implode( '|', $allowed ).')$#i', $filename ) )
                    {
                        copy( 'zip://'. $zip_file .'#'. $filename, ( $extract_to."/".basename( $full_name['name'] ) ) );
                        $file_list[] = $filename;
                    }   
                }
            }

            //Unzip ALL files (defaults to false)
            if( $all === true )
            {
                if( !$zip->extractTo( $extract_to ) )
                {
                    $zip->close();
                    return 'Unable to extract zip contents';
                }   
            }

            $zip->close();

            return implode( '<br />', $file_list );
        } 
        else 
        {
            return 'Error reading zip-archive!';
        }
    }   
}


if( !function_exists( 'extract_rar' ) )
{
	/**
	 * Extract the contents of a rar file to a directory
	 * Example:
	 * $rarfile = dirname( __FILE__ ) .'/FreeBird - Twisted Journeys.zip';
	 * extract_rar( $rarfile, './' );
	 *
	 * @access public
	 * @param string $rarfile to extract
	 * @param string $extract_to directory
	 * @return string
	 */
    function extract_rar( $rar_file, $extract_to )
    {
        if( !class_exists( 'RarArchive' ) )
        {
            return 'RarArchive class not found';
        }

        //Allowed file types used for glob selection
        $allowed = array( 'gif', 'jpg', 'png', 'mov', 'mp4', 'tiff', 'jpeg' );

        $rar_file = rar_open( $rar_file );
        if( $rar_file === FALSE )
        {
            return 'Unable to open RAR archive';
        }

        $entries = rar_list( $rar_file );
        if( $entries === FALSE )
        {
            return 'Unable to retrieve contents of rar file';
        }

        if( empty( $entries ) )
        {
            return 'No files found in archive';
        }

        $file_list = array();
        foreach( $entries as $entry )
        {
            $file_list[] = $entry->getName();

            $entry->extract( $extract_to );
        }

        rar_close( $rar_file );

        return implode( '<br />', $file_list );
    }   
}


/* End of file file-helper.php */
/* Location: application/rapid-helpers/file-helper.php */