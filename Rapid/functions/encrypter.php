<?php
/**
 * encrypter.php
 *
 * @version 1.0
 * @date 2/24/17 10:22 AM
 * @package rapidPHPCore
 */

if( !function_exists( 'encrypt' ) )
{
	/**
	 * @param $data
	 * @return string
	 * @throws Exception
	 */
	function encrypt( $data )
	{
		$encrypt = new \Rapid\Helpers\Encrypt( env( 'APP_KEY' ) );
		return $encrypt->encrypt( $data );
	}
}


if( !function_exists( 'decrypt' ) )
{
	/**
	 * @param $data
	 * @return mixed
	 * @throws Exception
	 */
	function decrypt( $data )
	{
		$encrypt = new \Rapid\Helpers\Encrypt( env( 'APP_KEY' ) );
		return $encrypt->decrypt( $data );
	}
}