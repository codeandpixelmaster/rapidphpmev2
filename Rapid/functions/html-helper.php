<?php
/**
 * html-helper.php
 * application/rapid-helpers/html-helper.php
 * Assorted HTML cleaning, formatting, and syntax rapid-helpers
 * Display output utilities such as time ago
 *
 * @version 1.0
 * @date 13 Aug 2012
 * @updated 25-Dec-2016
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** active_nav()            //compares current url against specified url, becomes active if matched
 ** close_tags()            //finds and closes opened HTML tags that weren't previously closed
 ** count_words()           //clear enough
 ** nicetime()              //Time ago display function
 ** blacklist()             //Nasty array of words you may not want people putting in comments/contact forms
 ** clean()                 //Removes what DB sanitizing adds for cleaner HTML output of db contents
 ** tense()                 //Adds/removes appropriate word"s" based on occurance count
 ** implode_normalize()     //Return a normalized string from an array ending in sentence contextual ending for last item
 ** flash()                 //Allows one time messages to be passed between pages using session vars
 ** load_modal()            //Simple loading of jquery.reveal modals
 ** excerpt()               //Chop chop at number of words or closest char count
 ** fixed_excerpt()         //Chop text string adding elipses as needed
 ** random_string()         //Generate random strings
 ** fb_share()              //Generate a FB Share option/link
 ** twitter_share()         //Generate a twitter share option/link
 ** create_slug()           //Generate URI friendly slugs from a string
 ** set_status_header()     //Outputs HTTP status codes + text
 ** redirect()              //Force a header redirect
 ** href()                  //Generate full http link
 **/

if( !defined( 'ROOT' ) ) exit( 'No direct script access allowed.' );


if( !function_exists( 'active_nav' ) )
{

    /**
     * Determine active nav state based on current page being viewed
     * Usage: Put <?php active_nav( array('file1.php', 'file1.php') ); ?> where class output is desired
     * ALso accepts single item rather than array <?php active_nav( 'single-file.php' ); ?>
     *
     * @param string|array $active Page values to classify as active if matched
     * @param bool|true $echo (defaults to true)
     * @param string $class to pass to display active class
     * @return string
     */
    function active_nav( $active, $echo = true, $class = ' class="active"' )
    {
        $show_active = false;
    
        if( is_array( $active ) )
        {
            foreach( $active as $a )
            {
                $a = trim( $a );
                if( !empty( $a ) && strpos( $_SERVER['REQUEST_URI'], $a ) )
                {
                    $show_active = true;
                }
            }
        }
        else
        {
            $active = trim( $active );
            if( $active == basename( $_SERVER['REQUEST_URI'] ) )
            {
                $show_active = true;
            }
            elseif( $active == 'home' && basename( $_SERVER['REQUEST_URI'] ) == str_replace( '/', '', DIRNAME ) )
            {
                $show_active = true;
            }
        }

        if( $show_active === true && $echo )
        {
            echo $class;
        }
        if( $show_active === true && !$echo )
        {
            return $class;
        }
    }
    
}


if( !function_exists( 'close_tags' ) )
{
    /**
     * Function to find and close all opened but not closed HTML tags in a string
     *
     * @access public
     * @param string $text
     * @return string text (with closed tags)
     */
    function close_tags( $text )
    {
        $patt_open    = "%((?<!</)(?<=<)[\s]*[^/!>\s]+(?=>|[\s]+[^>]*[^/]>)(?!/>))%";
        $patt_close    = "%((?<=</)([^>]+)(?=>))%";
        if( preg_match_all( $patt_open, $text, $matches ) )
        {
            $m_open = $matches[1];
            $m_close = array();
            if( !empty( $m_open ) )
            {
                preg_match_all( $patt_close, $text, $matches2 );
                $m_close = $matches2[1];
                $c_tags = array();
                if( count( $m_open ) > count( $m_close ) )
                {
                    $m_open = array_reverse( $m_open );
                    foreach( $m_close as $tag ) $c_tags[$tag]++;
                    foreach( $m_open as $k => $tag )
                    {
                        if( $c_tags[$tag]--<=0 )
                        {
                            $text.='</'.$tag.'>';   
                        }
                    }
                    
                } //end if count( $m_open ) > count( $m_close )
                
            } //end if !empty( $m_open )
            
        } //end if preg_match
        
        return $text;
    }
    
}    


if( !function_exists( 'count_words' ) )
{

    /**
     * Function to count words
     * @access public
     * @param string $words
     * @return int count
     */
    function count_words( $words = '' )
    {
        return str_word_count( $words );
    }
    
}


if( !function_exists( 'nicetime' ) )
{
    /**
     * Time ago function
     *
     * @access public
     * @param $date
     * @return string
     */
    function nicetime( $date )
    {
        if( empty( $date ) )
        {
            return "No date provided";
        }
    
        $periods = array( "second", "minute", "hour", "day", "week", "month", "year", "decade" );
        $lengths = array( "60", "60", "24", "7", "4.35", "12", "10" );
    
        $now = time();
        $unix_date = strtotime( $date );
    
           // check validity of date
        if( empty( $unix_date ) )
        {
            return "Bad date";
        }

        // is it future date or past date
        if( $now > $unix_date )
        {    
            $difference = $now - $unix_date;
            $tense = "ago";
        
        }
        else
        {
            $difference = $unix_date - $now;
            $tense = "from now";
        }
    
        for( $j = 0; $difference >= $lengths[$j] && $j < count( $lengths )-1; $j++ )
        {
            $difference /= $lengths[$j];
        }
    
        $difference = round( $difference );
    
        if( $difference != 1 )
        {
            $periods[$j].= "s";
        }
    
        return "$difference $periods[$j] {$tense}";
    }
    
}


if( !function_exists( 'blacklist' ) )
{
    /**
     * Blacklist function to prevent common obscenities from being displayed or logged as tags
     * List src: http://www.jomsocial.com/customize/addons/content-enhancements/profanity-filter-word-list
     *
     * @access public
     * @param string $input
     * @return bool
     */
    function blacklist( $input )
    {
        $blacklist = array( 'ass', 'ass lick', 'asses', 'asshole', 'assholes', 'asskisser', 'asswipe', 'balls', 'bastard', 'beastial', 'beastiality', 'beastility', 'beaver', 'belly whacker', 'bestial', 'bestiality', 'bitch', 'bitcher', 'bitchers', 'bitches', 'bitchin', 'bitching', 'blow job', 'blowjob', 'blowjobs', 'bonehead', 'boner', 'brown eye', 'browneye', 'browntown', 'bucket cunt', 'bull shit', 'bullshit', 'bum', 'bung hole', 'butch', 'butt', 'butt breath', 'butt fucker', 'butt hair', 'buttface', 'buttfuck', 'buttfucker', 'butthead', 'butthole', 'buttpicker', 'chink', 'circle jerk', 'clam', 'clit', 'cobia', 'cock', 'cocks', 'cocksuck', 'cocksucked', 'cocksucker', 'cocksucking', 'cocksucks', 'cooter', 'crap', 'cum', 'cummer', 'cumming', 'cums', 'cumshot', 'cunilingus', 'cunillingus', 'cunnilingus', 'cunt', 'cuntlick', 'cuntlicker', 'cuntlicking', 'cunts', 'cyberfuc', 'cyberfuck', 'cyberfucked', 'cyberfucker', 'cyberfuckers', 'cyberfucking', 'damn', 'dick', 'dike', 'dildo', 'dildos', 'dink', 'dinks', 'dipshit', 'dong', 'douche bag', 'dumbass', 'dyke', 'ejaculate', 'ejaculated', 'ejaculates', 'ejaculating', 'ejaculatings', 'ejaculation', 'fag', 'fagget', 'fagging', 'faggit', 'faggot', 'faggs', 'fagot', 'fagots', 'fags', 'fart', 'farted', 'farting', 'fartings', 'farts', 'farty', 'fatass', 'fatso', 'felatio', 'fellatio', 'fingerfuck', 'fingerfucked', 'fingerfucker', 'fingerfuckers', 'fingerfucking', 'fingerfucks', 'fistfuck', 'fistfucked', 'fistfucker', 'fistfuckers', 'fistfucking', 'fistfuckings', 'fistfucks', 'fuck', 'fucked', 'fucker', 'fuckers', 'fuckin', 'fucking', 'fuckings', 'fuckme', 'fucks', 'fuk', 'fuks', 'furburger', 'gangbang', 'gangbanged', 'gangbangs', 'gaysex', 'gazongers', 'goddamn', 'gonads', 'gook', 'guinne', 'hard on', 'hardcoresex', 'homo', 'hooker', 'horniest', 'horny', 'hotsex', 'hussy', 'jack off', 'jackass', 'jacking off', 'jackoff', 'jack-off', 'jap', 'jerk', 'jerk-off', 'jism', 'jiz', 'jizm', 'jizz', 'kike', 'kock', 'kondum', 'kondums', 'kraut', 'kum', 'kummer', 'kumming', 'kums', 'kunilingus', 'lesbian', 'lesbo', 'merde', 'mick', 'mothafuck', 'mothafucka', 'mothafuckas', 'mothafuckaz', 'mothafucked', 'mothafucker', 'mothafuckers', 'mothafuckin', 'mothafucking', 'mothafuckings', 'mothafucks', 'motherfuck', 'motherfucked', 'motherfucker', 'motherfuckers', 'motherfuckin', 'motherfucking', 'motherfuckings', 'motherfucks', 'muff', 'nigger', 'niggers', 'orgasim', 'orgasims', 'orgasm', 'orgasms', 'pecker', 'penis', 'phonesex', 'phuk', 'phuked', 'phuking', 'phukked', 'phukking', 'phuks', 'phuq', 'pimp', 'piss', 'pissed', 'pissrr', 'pissers', 'pisses', 'pissin', 'pissing', 'pissoff', 'prick', 'pricks', 'pussies', 'pussy', 'pussys', 'queer', 'retard', 'schlong', 'screw', 'sheister', 'shit', 'shited', 'shitfull', 'shiting', 'shitings', 'shits', 'shitted', 'shitter', 'shitters', 'shitting', 'shittings', 'shitty', 'slag', 'sleaze', 'slut', 'sluts', 'smut', 'snatch', 'spunk', 'twat', 'wetback', 'whore', 'wop' );
    
        if( in_array( $input, $blacklist ) )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}


if( !function_exists( 'clean' ) )
{
    /**
     * Function to clean data for nice output
     *
     * @access public
     * @param string $text
     * @return string
     */
    function clean( $text )
    {
        if( empty( $text ) )
        {
            return null;
        }
        if( is_array( $text ) )
        {
            $text = array_map( 'clean', $text );
        }
        else
        {
            $text = stripslashes( $text );
            $text = htmlentities( $text, ENT_QUOTES, 'UTF-8', false );
            $text = nl2br( $text );
            $text = urldecode( $text );
            $text = trim( $text );
        }
        return $text;
    }

}


if( !function_exists( 'tense' ) )
{

    /**
     * Function to provide case sensitive context
     * Context will always only have suffix for 0 or >2 counts
     * @param int $count
     * @param string $word
     * @param string $append
     * @return string
     */
    function tense( $count = 0, $word, $append = 's' )
    {
        if( $count == 0 || $count >= 2 )
        {
            $return = $word . $append;
        }
        else
        {
            $return = $word;
        }
        
        return $return;
    }
    
}


if( !function_exists( 'implode_normalize' ) )
{
    /**
     * Return a normalized string from an array ending in sentence contextual ending for last item
     *
     * @param $input
     * @param string $separator
     * @param string $last
     * @return string
     */
    function implode_normalize( $input, $separator = ', ', $last = ' or ' )
    {
        $out = '';
        $count = count( $input ) - 1;
        $start = 0;
        foreach( $input as $i )
        {
            $out .= ( $start < $count  ) ? $i . $separator : $last . $i;
            $start++;
        }
        return $out;
    }
}


if( !function_exists( 'flash' ) )
{
	/**
	 * Function to create and display error and success messages
	 * @access public
	 * @param string $name
	 * @param string $message
	 * @param string $class
	 * @return string message
	 */
    function flash( $name = '', $message = '', $class = 'success' )
    {
        if( !empty( $name ) )
        {
            //Ensure that a session exists (just in case)
            new \Rapid\Sessions();
            
            //No message, create it
            if( !empty( $message ) && empty( $_SESSION[$name] ) )
            {
                if( !empty( $_SESSION[$name] ) )
                {
                    unset( $_SESSION[$name] );
                }
                if( !empty( $_SESSION[$name.'_class'] ) )
                {
                    unset( $_SESSION[$name.'_class'] );
                }

                $_SESSION[$name] = $message;
                $_SESSION[$name.'_class'] = $class;
            }
            //Message exists, display it
            elseif( !empty( $_SESSION[$name] ) && empty( $message ) )
            {
                $class = !empty( $_SESSION[$name.'_class'] ) ? $_SESSION[$name.'_class'] : 'success';
                echo '<div class="alert-box radius '.$class.'" id="msg-flash">'.$_SESSION[$name].'</div>'; 
            
                unset( $_SESSION[$name] );
                unset( $_SESSION[$name.'_class'] );
            }
        }
    }

}


if( !function_exists( 'load_modal' ) )
{
	/**
	 * Function to easily display modal windows
	 * relies on jquery.reveal
	 *
	 * @param string $id The ID for this modal
	 * @param string $heading Modal title
	 * @param string $text Text for within modal window
	 * @param bool|true $show_close Show close window option
	 * @param bool|false $auto Open automatically onload
	 * @param int $size
	 * @return string
	 */
    function load_modal( $id, $heading = '', $text, $show_close = true, $auto = false, $size = 1 )
    {
        $sizes = array( 1 => 'small', 2 => 'medium', 3 => 'large', 4 => 'xlarge' );
        $modal = '';
        $modal .= '<div id="'.$id.'" class="reveal-modal '.$sizes[$size].'">' . PHP_EOL;
    
        if( !empty( $heading ) )
        {
            $modal .= '<h1>'.$heading.'</h1>' .PHP_EOL;
        }
    
        $modal .= $text;
    
        if( $show_close )
        {
            $modal .= PHP_EOL . '<a class="close-reveal-modal">&#215;</a>' . PHP_EOL;
            $prevent_close = '';
        }
        else
        {
            $prevent_close = '{ closeonbackgroundclick: false }';
        }
    
        $modal .= '</div>' . PHP_EOL;
    
        if( $auto )
        {
            $modal .= '<script>        
            $(window).load(function() {
                $("#'.$id.'").reveal('.$prevent_close.');
            });
            </script>';
        }
    
        return $modal;
    }

}


if( !function_exists( 'excerpt' ) )
{
	/**
	 * Function to generate excerpts
	 *
	 * @access public
	 * @param string $text
	 * @param int $words number of words to break at
	 * @param string $end elipses to use when truncating
	 * @return string
	 */
    function excerpt( $text, $words = 300, $end = '...' )
    {
        //Strip tags out of here
        $text = clean( $text );
        $text = str_replace( array( '<br>', '<br />', "\n", "\r\n" ), ' ', $text );
        $text = strip_tags( $text );
        // split the string by spaces into an array
        $split = explode( ' ', $text );

        if( count( $split ) > $words )
        {
            // rebuild the excerpt back into a string
            $text = join( ' ', array_slice( $split, 0, $words ) );
            $text = rtrim( $text, ',' ) . $end;
        }
        
        // append the ending, limit it, and return
        return $text;
    }
    
}


if( !function_exists( 'fixed_excerpt' ) )
{
	/**
	 * Function to generate fixed length excerpts
	 *
	 * @access public
	 * @param string $text
	 * @param int $limit character limit
	 * @param string $end elipses to use when truncating
	 * @return string
	 */
    function fixed_excerpt( $text, $limit = 120, $end = '...' )
    {

        //Clean up the text
        $text = strip_tags( trim( $text ) );

        //Add elipses or other endings as necessary
        if( strlen( $text ) > $limit )
        {
            $text = substr( $text, 0, $limit-strlen( $end ) ) . $end;
        }

        // append the ending, limit it, and return
        return $text;
    }
}


if( !function_exists( 'random_string' ) )
{
	/**
	 * Function to generate solid random string for paypal IPN
	 * Also used to generate unique IDs on email message parsing functions
	 *
	 * @param int $length
	 * @param bool|true $echo
	 * @return int|string
	 */
    function random_string( $length = 40, $echo = true )
    {
        $ident = mt_rand(20000,999999);
        $length = ( $length - strlen( $ident ) );
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $char_len = strlen( $characters );
        $string = $ident;    
        for ($p = 0; $p < $length; $p++)
        {
            $string .= $characters[mt_rand( 0, ($char_len-2) )];
        }
        if( $echo )
        {
            echo $string;
        }
        else
        {
            return $string;
        }
    }
    
}


if( !function_exists( 'fb_share' ) )
{
	/**
	 * Output consistent facebook share link
	 *
	 * @param string $url
	 * @param string $image
	 * @param string $title
	 * @param string $text
	 * @return string
	 */
    function fb_share( $url = BASE, $image = '', $title = 'Like us on Facebook', $text = '' )
    {

        $title = urlencode( $title );
        $url = urlencode( $url );
    
        if( empty( $text ) )
        {
            $text = 'Check out '. SITENAME . '!';
        }
    
        $summary = urlencode( $text );
        $image = urlencode(  $image );
    
        return '<a class="actions submit-button facebook" onClick="window.open(\'http://www.facebook.com/sharer.php?s=100&amp;p[title]='.$title.'&amp;p[summary]='.$summary.'&amp;p[url]='.$url.'&amp;&amp;p[images][0]='.$image.'\',\'sharer\',\'toolbar=0,status=0,width=548,height=325\');" title="Share on Facebook">Share on Facebook</a>';
    
    }

}


if( !function_exists( 'twitter_share' ) )
{
	/**
	 * Function to output share on twitter link
	 *
	 * @access public
	 * @return string $username
	 */
    function twitter_share( $username )
    {
        $status = urlencode( 'Check out @'.$username.' at '.BASE );
        return '<a href="http://twitter.com/home?status='.$status.'" title="Click to share this post on Twitter" target="_blank">Share on Twitter</a>';
    }

}


if( !function_exists( 'create_slug' ) )
{
	/**
	 * Generate a URI friendly slug from a string
	 *
	 * @access public
	 * @param $string
	 * @param int $word_limit
	 * @return string
	 */
    function create_slug( $string, $word_limit = 6 )
    {
        $slug = explode( " ",$string );
        $slug = implode( " ", array_splice( $slug, 0, $word_limit ) );
        $slug = str_replace( array( "'", "(",")", "-", "!", "&" ), "", $slug );
        $slug = preg_replace( '/[^A-Za-z0-9]+/', '-', $slug );
        $slug = ltrim( $slug, '-' );
        $slug = rtrim( $slug, '-' );
        return strtolower( $slug );
    }

}


if( !function_exists( 'format_size' ) )
{
	/**
	 * Function to format filesize into more readable info
	 * $size passed generally retrieved from php's filesize()
	 * @access public
	 * @param int $size
	 * @return string
	 */
    function format_size( $size )
    {
        $sizes = array( " Bytes", " KB", " MB", " GB", " TB", " PB", " EB", " ZB", " YB" );
        if( $size == 0 )
        {
            return( 'n/a' );
        }
        else
        {
            return ( round( $size/pow( 1024, ( $i = floor( log( $size, 1024 ) ) ) ), 2 ) . $sizes[$i] );
        }
    }
       
}


/**
 * Function to set HTTP status headers
 * Mainly used for API requests
 * @access public
 * @param int $code
 * @param string $text
 * @return string
 */
function set_status_header( $code = 200, $text = '' )
{
    $response = new Rapid\StatusResponse();
    $response->set_status_header( $code, $text );
}


if( !function_exists( 'redirect' ) )
{

	/**
	 * Function to handle redirects if the headers haven't already been sent
	 * OR even if they have!
	 * Usage:
	 * redirect( 'http://yourplace.com' );
	 *
	 * @access public
	 * @param string $to Location to redirect
	 * @param int $duration Duration before meta refresh
	 * @return null
	 */
    function redirect( $to, $duration = 0 )
    {
        if( !headers_sent() )
        {
            header( 'Location: '. $to );
            exit;
        }
        else
        {
            echo '<META http-equiv="refresh" content="'.$duration.';URL=' . $to . '">';
        }
    }
}


if( !function_exists( 'href' ) )
{
	/**
	 * Function to generate link inc full BASE uri
	 * @access public
	 * @param string $link
	 * @return string
	 */
    function href( $link = '' )
    {
        return BASE . ( !empty( $link ) ? SEP . trim( $link ) : '' );
    }
}


/* End of file html-helper.php */
/* Location: application/rapid-helpers/html-helper.php */