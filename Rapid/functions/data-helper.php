<?php
/**
 * data-helper.php
 * application/functions/data-helper.php
 * Assorted data cleaning, formatting, and syntax rapid-helpers
 * @version 1.2
 * @date 26 Aug 2015
 * @updated 24-Feb-2017
 * @package RapidPHPMe
 *
 * Table of contents:
 *
 ** _e()                    //returns xss filtered data
 ** xecho()                 //echos _e output
 ** returns()               //returns $_GET or $_POST value by key
 ** right_now()             //generates timestamps
 ** get()                   //Get $_GET value by key
 ** post()                  //Get $_POST value by key
 ** getParameter()			//Retrieve $_GET or $_POST value by key
 ** session()               //Get $_SESSION value by key
 ** cookie()                //Get $_COOKIE value by key
 ** env()					//Get and set defaults for environmental variables
 ** startsWith()			//Find out if a string starts with ...
 ** endsWith()				//Find out if a string ends with ...
 ** is_serial               //Check if a string is serialized
 ** is_ajax()               //Checks if scripts are loaded via ajax or not
 ** json()                  //Function to save a few chars returning json
 ** parseJson()				//Convert raw JSON input into an associative array
 ** random_id()				//Generate a really random ID
 **/


/**
 * XSS mitigation functions
 * See https://www.owasp.org/index.php/PHP_Security_Cheat_Sheet
 * @access public
 * @param mixed
 * @return string
 */
function _e( $data )
{
	return htmlspecialchars( $data, ENT_QUOTES | ENT_HTML401, 'UTF-8' );
}


/**
 * @param $data
 */
function xecho( $data )
{
	echo _e( $data );
}


if( !function_exists( 'returns' ) )
{
	/**
	 * @param $input
	 * @return string
	 */
	function returns( $input )
	{
		$return = '';
		if( isset( $_POST[$input] ) )
		{
			return _e( $_POST[$input] );
		}
		elseif( isset( $_GET[$input] ) )
		{
			return _e( $_GET[$input] );
		}
		elseif( isset( $_SESSION[$input] ) )
		{
			return _e( $_SESSION[$input] );
		}
		return $return;
	}
}


if( !function_exists( 'right_now' ) )
{
	/**
	 * Function to output mysql style timestamps
	 * @access public
	 * @param none
	 * @return string timestamp
	 */
	function right_now()
	{
		return date( "Y-m-d H:i:s", mktime( date('H'), date('i'), date('s') ) );
	}

}


if( !function_exists( 'get' ) )
{
	/**
	 * Function to check for $_GET params and return value if exists
	 * To use as a replacement within an "isset( $_GET['param'] )" you must instead
	 * use the following format:
	 *
	 * if( null !== get( 'param' ) )
	 *
	 * @param $key
	 * @param null $default
	 * @return null
	 */
	function get( $key, $default = null )
	{
		if( isset( $_GET[$key] ) )
		{
			return $_GET[$key];
		}
		return $default;
	}
}


if( !function_exists( 'post' ) )
{
	/**
	 * Function to check for $_POST params and return value if exists
	 * To use as a replacement within an "isset( $_POST['param'] )" you must instead
	 * use the following format:
	 *
	 * if( null !== post( 'param' ) )
	 *
	 * @param $key
	 * @param null $default
	 * @return null
	 */
	function post( $key, $default = null )
	{
		if( isset( $_POST[$key] ) )
		{
			return $_POST[$key];
		}
		return $default;
	}
}


if( !function_exists( 'getParameter' ) )
{
	/**
	 * Return only $_GET or $_POST parameters
	 *
	 * @param $key
	 * @param null $default
	 * @return null
	 */
	function getParameter( $key, $default = null )
	{
		if( isset( $_GET[$key] ) )
		{
			return $_GET[$key];
		}
		if( isset( $_POST[$key] ) )
		{
			return $_POST[$key];
		}
		return $default;
	}
}


if( !function_exists( 'session' ) )
{
	/**
	 * Function to check for $_SESSION params and return value if exists
	 * To use as a replacement within an "isset( $_SESSION['param'] )" you must instead
	 * use the following format:
	 *
	 * if( null !== session( 'param' ) )
	 *
	 * @param $key
	 * @param null $default
	 * @return null
	 */
	function session( $key, $default = null )
	{
		if( isset( $_SESSION[$key] ) )
		{
			return $_SESSION[$key];
		}
		return $default;
	}
}


if( !function_exists( 'cookie' ) )
{
	/**
	 * Function to check for $_COOKIE params and return value if exists
	 * To use as a replacement within an "isset( $_COOKIE['param'] )" you must instead
	 * use the following format:
	 *
	 * if( null !== cookie( 'param' ) )
	 *
	 * @param $key
	 * @param null $default
	 * @return null
	 */
	function cookie( $key, $default = null )
	{
		if( isset( $_COOKIE[$key] ) )
		{
			return $_COOKIE[$key];
		}
		return $default;
	}
}


if( !function_exists( 'env' ) )
{
	/**
	 * Gets the value of an environment variable. Supports boolean, empty and null.
	 *
	 * @param  string  $key
	 * @param  mixed   $default
	 * @return mixed
	 */
	function env( $key, $default = null )
	{
		$value = getenv( $key );

		if( $value === false )
		{
			return $default;
		}

		switch( strtolower( $value ) )
		{
			case 'true':
			case '(true)':
				return true;
			case 'false':
			case '(false)':
				return false;
			case 'empty':
			case '(empty)':
				return '';
			case 'null':
			case '(null)':
				return null;
		}

		if( strlen( $value ) > 1 && startsWith( $value, '"' ) === '"' && endsWith($value, '"') )
		{
			return substr( $value, 1, -1 );
		}

		return $value;
	}
}


if( !function_exists( 'startsWith' ) )
{
	/**
	 * @src http://stackoverflow.com/a/10473026
	 * @param $haystack
	 * @param $needle
	 * @return bool
	 */
	function startsWith( $haystack, $needle )
	{
		// search backwards starting from haystack length characters from the end
		return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== false;
	}
}


if( !function_exists( 'endsWith' ) )
{
	/**
	 * @src http://stackoverflow.com/a/10473026
	 * @param $haystack
	 * @param $needle
	 * @return bool
	 */
	function endsWith( $haystack, $needle )
	{
		// search forward starting from end minus needle length characters
		return $needle === "" || (($temp = strlen($haystack) - strlen($needle)) >= 0 && strpos($haystack, $needle, $temp) !== false);
	}
}


if( !function_exists( 'is_serial' ) )
{
	/**
	 * Check if a string is serialized
	 * @param string $string
	 * @return bool
	 */
	function is_serial( $string )
	{
		return ( @unserialize($string) !== false || $string == 'b:0;' );
	}
}


if( !function_exists( 'is_ajax' ) )
{
	/**
	 * Function to determine if a script has been requested via ajax or not
	 * @access public
	 * @param none
	 * @return bool
	 */
	function is_ajax()
	{
		if( isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) && strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) == 'xmlhttprequest' )
		{
			return true;
		}
		elseif( isset( $_SERVER['HTTP_X_PJAX'] ) )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}


if( !function_exists( 'json' ) )
{
	/**
	 * Function to save a few chars returning json
	 * @access public
	 * @param $data
	 * @return string
	 */
	function json( $data )
	{
		return json_encode( $data );
	}
}


if( !function_exists( 'parseJson' ) )
{
	/**
	 * Parse JSON
	 *
	 * Convert raw JSON input into an associative array.
	 *
	 * @param $input
	 * @return array|string
	 */
	function parseJson( $input )
	{
		if( function_exists( 'json_decode' ) )
		{
			$result = json_decode( $input, true );
			if( json_last_error() === JSON_ERROR_NONE )
			{
				return $result;
			}
		}
		return $input;
	}
}


if( !function_exists( 'random_id' ) )
{
	/**
	 * Function to generate a super random ID
	 * @access public
	 * @param int $length
	 * @return string
	 */
	function random_id( $length = 30 )
	{
		return substr( strrev( str_shuffle( substr( str_rot13( bin2hex( hex2bin( sha1( metaphone( md5( time() ) ) ) ) ) ), 0, time() ) ) ), 0, $length );
	}
}

/* End of file data-helper.php */
/* Location: application/functions/data-helper.php */