<?php
/**
 * Function to write debug messages to a file
 * This should only be used for development purposes
 * @access public
 * @param string $message
 * @return none
 */

function log_message( $message )
{
    //Log the errors to a file as necessary
    if( defined( 'RUN_LOGS' ) && ( RUN_LOGS !== false ) && is_dir( LOGS ) && is_writable( LOGS ) )
    {
        $logfile = LOGS . '/debug-log-'.date( 'Y-m-d' ).'.php';
        $log_message = '';
        
        $backtrace = debug_backtrace();
        $data = [];
        foreach( $backtrace as $trace )
        {
            //Skip this file
            if( ( isset( $trace['file'] ) && $trace['file'] == __FILE__ ) || count( $trace ) == 0 || count( $data ) == 1 )
            {
                continue;
            }

            if( $trace['function'] == 'error_handler' && count( $data ) == 0 )
            {
                continue;
            }

            $params = array(
                'file' => isset( $trace['file'] ) ? $trace['file'] : '',
                'line' => isset( $trace['line'] ) ? $trace['line'] : '',
                'function' => $trace['function']
            );
            if( !empty( $trace['args'] ) )
            {
                $params['args'] = $trace['args'];
            }
            $data[] = $params;
        }

        if( !file_exists( $logfile ) )
        {
            $log_message .= "<"."?php if( __FILE__ == \$_SERVER['DOCUMENT_ROOT'] . \$_SERVER['SCRIPT_NAME'] ) exit( 'No direct script access allowed.' ); ?".">\n\n";
        }
        
        $handle = fopen( $logfile, 'a' ) or trigger_error( 'Cannot open file:  '.$logfile );

        //If a max size is set for the logfile, we need to make sure we don't exceed it
        if( defined( LOG_SIZE ) && file_exists( $logfile ) )
        {
            if( filesize( $logfile ) > ( 1024 * 1024 * LOG_SIZE ) )
            {
                $f = fopen( $logfile, 'w' );
                fclose( $f );
            }
        }
        if( !$handle )
        {
            return;
        }
        
        //Handle varying types of potential messages here
        if( is_object( $message ) || is_array( $message ) )
        {
            $log_message .= print_r( $message, true );
        }
        else
        {
            $log_message .= $message;
        }
        
        $log_message .= PHP_EOL . print_r( $data, true );

        $seperator = PHP_EOL . str_repeat( '-', 50 ) . PHP_EOL;
        $log_message = strip_tags( $log_message, '<p><br>' );
        $message = str_replace( array( '<p>', '</p>', '<br />', '<br>' ), PHP_EOL, $log_message ) . $seperator;
        fwrite( $handle, $message );
        fclose( $handle );
    }
}
//end log_message()