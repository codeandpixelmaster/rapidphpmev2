<?php
/**
 * Control.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

namespace Rapid;

class Control {

    /**
     * @type bool|SimplePDO
     */
    public $db = false;

    /**
     * @type bool|userClass
     */
    public $users = false;

    /**
     * @type Models
     */
    public $model;
    
    
    public function __construct()
    {
        global $userclass;
                
        $this->db = Options::getDB();
        
        $this->users = $userclass;
    }
    //end construct
    
    
    /**
     * Function to extend $this->view() accessibility to explicit controller classes
     * which extend Control
     * @access public
     * @param string $file
     * @param array $data
     * @param bool $cache
     * @param bool $flush (the cache)
     * @return string
     */
    public function view( $file, $data = null, $cache = false, $flush = false )
    {
        Models::view_file( $file, $data, $cache, $flush );
    }
    //end view()
    
}
//end class Control