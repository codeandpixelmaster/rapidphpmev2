<?php
/**
 * Sessions.php
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @updated 23-Feb-2017
 * @package RapidPHPMe Core
 **/

namespace Rapid;

class Sessions {

    /**
     * @type null|Sessions
     */
    private static $inst = null;

    /**
     * @type string
     */
    private static $sessions_table = 'sessions';

    /**
     * @type bool
     */
    private static $use_db = false;


    /**
     * Sessions constructor.
     */
    public function __construct()
    {
        if( Options::get_config( 'session_use_db' ) === true )
        {
            self::$use_db = true;
        }

        $this->setupTable();
        //Auto start the session if this class gets initiated at all
        $this->force_session();
    }


    /**
     * @return null|Sessions
     */
    public static function init()
    {
        if( self::$inst == null )
        {
            self::$inst = new Sessions();
        }
        return self::$inst;
    }
    //end init()


    /**
     * Function to reliably start sessions
     * @access public
     * @param none
     * @return none
     */
    public function force_session()
    {

        if( self::$use_db )
        {
            session_set_save_handler(
                array( __CLASS__, 'open_session' ),
                array( __CLASS__, 'close_session' ),
                array( __CLASS__, 'read_session' ),
                array( __CLASS__, 'write_session' ),
                array( __CLASS__, 'destroy_session' ),
                array( __CLASS__, 'clean_sessions' )
            );

            register_shutdown_function('session_write_close');
        }

        if( ( !session_id() || !strlen( session_id() ) ) )
        {
            session_start();
        }
        else
        {
            //Provide a 5% chance of the session being regenerated for security
            if( rand( 1, 100 ) <= 5 && !headers_sent() )
            {
                // Create new session without destroying the old one
                session_regenerate_id( false );

                // Grab current session ID and close both sessions to allow other scripts to use them
                $new_session = session_id();
                session_write_close();

                // Set session ID to the new one, and start it back up again
                session_id( $new_session );
                session_start();
            }
        }
    }


    public static function open_session() {}

    public static function close_session() {}


    /**
     * @param $sid
     * @return string
     */
    public static function read_session( $sid )
    {
        $session = ConfigLoader::db()->get_row( "SELECT `data` FROM ". self::$sessions_table ." WHERE id = ? LIMIT 1", array( $sid ) );
        if( !empty( $session ) && isset( $session->data ) )
        {
            return $session->data;
        }
        return '';
    }


    /**
     * @param $sid
     * @param $data
     * @return int
     */
    public static function write_session( $sid, $data )
    {
        $sql = ConfigLoader::db()->query( "REPLACE INTO ". self::$sessions_table ." (`id`, `data`) VALUES(?, ?)", array( $sid, $data ) );
        return true;
    }


    /**
     * @param $sid
     * @return int
     */
    public static function destroy_session( $sid )
    {
        $cleared = ConfigLoader::db()->delete( self::$sessions_table, array( 'id' => $sid ), 1 );
        $_SESSION = array();
        return $cleared;
    }


    /**
     * @param $expire
     * @return int
     */
    public static function clean_sessions( $expire )
    {
        $sql = ConfigLoader::db()->query( "DELETE FROM ". self::$sessions_table ." WHERE DATE_ADD(last_accessed, INTERVAL ? SECOND) < NOW()", array( (int)$expire ) );
        return ConfigLoader::db()->affected();
    }


    /**
     * Creates the sessions table if it doesn't already exist
     */
    private function setupTable()
    {
        if( self::$use_db && ConfigLoader::db() !== false && !ConfigLoader::db()->table_exists( self::$sessions_table ) )
        {
            $sql = "CREATE TABLE IF NOT EXISTS `sessions` (
              `id` char(32) NOT NULL,
              `data` text,
              `last_accessed` timestamp NOT NULL,
              PRIMARY KEY(`id`)
            )";
            ConfigLoader::db()->query( $sql );
        }
    }
}

/* End of file Sessions.php */
/* Location: /application/core/Sessions.php */