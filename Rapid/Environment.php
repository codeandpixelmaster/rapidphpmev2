<?php
/**
 * Environment.php
 *
 * @author Bennett Stone
 * @version 1.1
 * @date 26-Mar-2015
 * @updated 04-Jul-2016
 * @package RapidPHPMe Core
 **/

namespace Rapid;

class Environment {
    
    public function __construct()
    {
        //Add environment checker
        require_once( ROOT . SEP .'application' . SEP . 'config'. SEP . 'environments.php' );

        $env_file = ROOT . '/.env';

        if( file_exists( $env_file ) )
        {
            putenv( 'APP_ENV=' . ENVIRONMENT );
            try {
                ( new \Dotenv\Dotenv( ROOT, '.env' ) )->overload();
            } catch (\Dotenv\Exception\InvalidPathException $e) {
                //Do nothing.  No .env files set
            }
        }
    }
    //end construct

}
//end class Environment