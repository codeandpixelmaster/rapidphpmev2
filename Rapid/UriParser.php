<?php
/**
 * UriParser.php
 * Main initialization file for RapidPHP Framework
 *
 * @author Bennett Stone
 * @version 1.1
 * @date 26-Mar-2015
 * @updated 25-Apr-2015
 * @package RapidPHPMe Core
 **/

namespace Rapid;

class UriParser {

	private $request;

	private $final_path;

	private $url_path;

	private $parameters;

	private $class;

	private $function;

	private $routes;

	/* Retrieve the values requested, and pass into a variable for use in page retrieval */
	public function __construct()
	{
		$this->request = $_SERVER['REQUEST_URI'];

		/* Remove the directory variable if the site is in a subdirectory */
		if( defined( 'SUBDIR' ) && SUBDIR === true )
		{
			$this->request  = str_replace( Options::get_config( 'dirname' ). '/', '', $_SERVER['REQUEST_URI'] );
		}

		$this->routes = Options::get_routes();
	}


	/**
	 * Function to return compiled params for use in Control class
	 * @access public
	 * @param none
	 * @return array
	 */
	public function get_params()
	{
		return $this->get_query_var();
	}
	//end get_params()


	/**
	 * Function used to ensure no leading or trailing slashes in uri
	 * for parsing correctly
	 * @access private
	 * @param string $string
	 * @return string
	 */
	private function strip_trailing( $string )
	{
		$string = rtrim( $string, '/' );
		$string = ltrim( $string, '/' );
		return $string;
	}
	//end strip_trailing()


	/**
	 * Function to parse and appropriate query URIs
	 * Adjusted to be framework specific
	 * @access private
	 * @param string $url
	 * @return array
	 *
	 * @author Gajus Kuizinas <g.kuizinas@anuary.com>
	 * @copyright Anuary Ltd, http://anuary.com
	 * @version 1.0.0 (2011 12 06)
	 */
	private function url_route( $url )
	{

		$this->final_path = FALSE;

		//Trim any trailing slashes
		$url = $this->strip_trailing( $url );

		$this->url_path = explode( '/', $url );
		$url_path_length = count( $this->url_path );

		foreach( $this->routes as $original_path => $filter )
		{
			// reset the parameters every time in case there is partial match
			$this->parameters = array();

			foreach( $filter as $i => $key )
			{

				/**
				 * If the $key is mapped in $routes AND the URI path contains the string
				 * then we'll add it as a parameter to be used for $_GET
				 */
				if( strpos( $key, ':' ) === 0 && ( isset( $this->url_path[$i] ) || !empty( $this->url_path[$i] ) ) )
				{
					$this->parameters[substr( $key, 1 )] = $this->url_path[$i];
				}
				//this filter is irrelevent
				elseif( isset( $this->url_path[$i] ) && $key != $this->url_path[$i] )
				{
					continue 2;
				}
			}

			$this->final_path = $original_path;

			break;
		}
		if( empty( $this->final_path ) )
		{
			return array( 'path' => implode( '/', $this->url_path ) );
		}
		else
		{
			return array( 'path' => $this->final_path, 'parameters' => $this->parameters );
		}
	}
	//end url_route()


	/**
	 * Function to check a URI for possible class/function matching
	 * @access private
	 * @param string $uri
	 * @return array
	 */
	private function class_match( $uri )
	{
		$uri = $this->strip_trailing( $uri );
		$uri = explode( '/', $uri );
		$data = array(
			'class' => null,
			'function' => null,
		);

		if( isset( $uri[0] ) )
		{
			$data['class'] = ucwords( $uri[0] );
		}
		if( isset( $uri[1] ) )
		{
			$data['function'] = str_replace( '-', '_', $uri[1] );
		}
		return $data;
	}
	//end class_match()


	/**
	 * Get query variable and return as requested as array with $page variable
	 * @access public
	 * @param none
	 * @return array
	 */
	private function get_query_var()
	{

		//Reassign the request for use
		$uri = $this->request;

		if( strncmp( $uri, '?/', 2 ) === 0 )
		{
			//Remove the first ?/
			$uri = substr( $uri, 2 );
		}

		/*
		 * Now split at the ?
		 * The first part will be uri
		 * The Second part will be actual query string
		 */
		$parts = preg_split( '#\?#i', $uri, 2 );

		$uri = $parts[0];
		if( isset( $parts[1] ) )
		{
			$_SERVER['QUERY_STRING'] = $parts[1];
			parse_str( $_SERVER['QUERY_STRING'], $_GET );
		}
		else
		{
			$_SERVER['QUERY_STRING'] = '';
			$_GET = array();
		}

		//Evaluate the URI based on available routes
		$segments = $this->url_route( $uri );

		//Return a potential class/function response to try to match against
		$class_match = $this->class_match( $uri );

		if( strpos( $segments['path'], '/' ) !== false )
		{

			//Assign a default value to the $page
			$segments['path'] = explode( '/', $segments['path'] );

			//The last value of the path should always be the view file to be loaded
			$page = array_pop( $segments['path'] );

			//Default $view_directory is VIEWS
			$view_directory = VIEWS .implode( '/',  $segments['path'] );
		}
		else
		{
			//Assign default views directory
			$view_directory = VIEWS;
			//Handle normal path to view file
			$page = $segments['path'];
		}

		//Pass any defined $_GET params back to the $_GET vars
		if( isset( $segments['parameters'] ) && !empty( $segments['parameters'] ) )
		{
			foreach( $segments['parameters'] as $get => $value )
			{
				$_GET[$get] = $value;
			}
		}

		//If nothing gets passed for the $page, assume it's home
		if( empty( $page ) )
		{
			$page = 'home';
		}

		$firstpath = isset( $segments['path'] ) && is_array( $segments['path'] ) ? array_shift( $segments['path'] ) : $segments['path'];
		
		//If there is no firstpath, assume a "class Global" class variable
		if( empty( $firstpath ) )
		{
		    $firstpath = 'Common';
		}
		
		//Assign classmatch params if they don't exist for checking
		if( empty( $class_match['class'] ) && empty( $class_match['function'] ) )
		{
		    $class_match = array(
                'class' => $firstpath, 
                'function' => $page, 
		    );
		}

		return array(
			'pieces' => $segments,
			'firstpath' => $firstpath,
			'page' => $page,
			'directory' => $view_directory,
			'class_segment' => $class_match,
		);
	}

}
//end class UriParser