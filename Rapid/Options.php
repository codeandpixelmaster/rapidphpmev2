<?php
/**
 * Options.php
 * Static single loader for accessible configuration initialization
 *
 *
 * @author Bennett Stone
 * @version 1.0
 * @date 26-Mar-2015
 * @package RapidPHPMe Core
 **/

namespace Rapid;

use Rapid\Helpers\Timer;

class Options {

	public static $environment;

	public static $base_path = \ROOT;

	public static $core_files;

	public static $subfolder = '';

	private static $initialized = false;

	public static $options = array(
		'config' => array(),
		'database' => array(),
		'cache' => array(),
		'constants' => array(),
		'autoload' => array(),
		'routes' => array(),
		'views' => array(),
		'controllers' => array(),
		'filesystems' => array(),
		'mail' => array(),
	);


	private static function init()
	{
		if( !self::$initialized )
		{
			if( defined( 'ENVIRONMENT' ) )
			{
				self::$environment = trim( strtolower( ENVIRONMENT ) );
			}

			self::$core_files = CONFIG;

			self::$subfolder = CONFIG  . SEP . 'environments'. SEP . self::$environment . SEP;

			self::$initialized = true;
		}
	}
	//end init()


	public static function get_config( $return_var = '' )
	{
		Options::init();

		if( !file_exists( self::$core_files . 'config.php' ) )
		{
			throw new \Exception( 'The configuration file config.php does not exist.' );
		}

		//Prevent this from running if already set
		if( !empty( self::$options['config'] ) )
		{
			if( !empty( $return_var ) && isset( self::$options['config'][$return_var] ) )
			{
				return self::$options['config'][$return_var];
			}
			return self::$options['config'];
		}

		$base_config = include( self::$core_files . 'config.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'config.php' ) )
		{
			$base_config = array_replace_recursive( $base_config, include( $file_path ) );
		}

		self::$options['config'] = $base_config;

		//Check to see if we've requested a specific variable, and if it exists return it
		if( !empty( $return_var ) && isset( $base_config[$return_var] ) )
		{
			return $base_config[$return_var];
		}

		return $base_config;
	}
	//end get_config()


	public static function get_database( $return_var = '' )
	{
		Options::init();

		if( !file_exists( self::$core_files . 'database.php' ) )
		{
			throw new \Exception( 'The configuration file config.php does not exist.' );
		}

		$database = include( self::$core_files . 'database.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'database.php' ) )
		{
			$database = array_replace_recursive( $database, include( $file_path ) );
		}

		self::$options['database'] = $database;

		//Prevent this from running if already set
		if( !empty( self::$options['database'] ) )
		{
			if( !empty( $return_var ) && isset( self::$options['database'][$return_var] ) )
			{
				return self::$options['database'][$return_var];
			}
			return self::$options['database'];
		}

		//Check to see if we've requested a specific variable, and if it exists return it
		if( !empty( $return_var ) && isset( $database[$return_var] ) )
		{
			return $database[$return_var];
		}

		return $database;
	}
	//end get_database()


	/**
	 * @return bool|\SimplePDO
	 */
	public static function getDB()
	{
		return ConfigLoader::db();
	}


	/**
	 * @param string $return_var
	 * @return array|mixed
	 * @throws \Exception
	 */
	public static function get_cache( $return_var = '' )
	{
		Options::init();

		if( !file_exists( self::$core_files . 'cache.php' ) )
		{
			throw new \Exception( 'The configuration file cache.php does not exist.' );
		}

		//Prevent this from running if already set
		if( !empty( self::$options['cache'] ) )
		{
			if( !empty( $return_var ) && isset( self::$options['cache'][$return_var] ) )
			{
				return self::$options['cache'][$return_var];
			}
			return self::$options['cache'];
		}

		$cache = include( self::$core_files . 'cache.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'cache.php' ) )
		{
			$cache = array_replace_recursive( $cache, include( $file_path ) );
		}

		self::$options['cache'] = $cache;

		//Check to see if we've requested a specific variable, and if it exists return it
		if( !empty( $return_var ) && isset( $cache[$return_var] ) )
		{
			return $cache[$return_var];
		}

		return $cache;
	}
	//end get_cache()


	public static function get_constants( $return_var = '' )
	{
		Options::init();

		if( !file_exists( self::$core_files . 'constants.php' ) )
		{
			throw new \Exception( 'The configuration file constants.php does not exist.' );
		}

		//Prevent this from running if already set
		if( !empty( self::$options['constants'] ) )
		{
			if( !empty( $return_var ) && isset( self::$options['constants'][$return_var] ) )
			{
				return self::$options['constants'][$return_var];
			}
			return self::$options['constants'];
		}

		$constants = include( self::$core_files . 'constants.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'constants.php' ) )
		{
			$constants = array_replace_recursive( $constants, include( $file_path ) );
		}

		self::$options['constants'] = $constants;

		//Check to see if we've requested a specific variable, and if it exists return it
		if( !empty( $return_var ) && isset( $constants[$return_var] ) )
		{
			return $constants[$return_var];
		}

		return $constants;
	}
	//end get_constants()


	public static function get_autoload( $return_var = '' )
	{
		Timer::start( 'retrieving autoload list' );
		Options::init();


		if( !file_exists( self::$core_files . 'autoload.php' ) )
		{
			throw new \Exception( 'The configuration file autoload.php does not exist.' );
		}

		//Prevent this from running if already set
		if( !empty( self::$options['autoload'] ) )
		{
			Timer::start( 'returning existing autoload list' );
			if( !empty( $return_var ) && isset( self::$options['autoload'][$return_var] ) )
			{
				return self::$options['autoload'][$return_var];
			}
			return self::$options['autoload'];
		}

		$autoload = include( self::$core_files . 'autoload.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'autoload.php' ) )
		{
			$autoload = array_replace_recursive( $autoload, include( $file_path ) );
		}

		Timer::start( 'returning non-existing autoload list' );
		self::$options['autoload'] = $autoload;

		//Check to see if we've requested a specific variable, and if it exists return it
		if( !empty( $return_var ) && isset( $autoload[$return_var] ) )
		{
			Timer::start( 'returning single autoload var' );
			return $autoload[$return_var];
		}

		return $autoload;
	}
	//end get_autoload()


	public static function get_routes()
	{
		Timer::start( 'retrieving routes' );
		Options::init();

		if( !file_exists( self::$core_files . 'routes.php' ) )
		{
			throw new \Exception( 'The configuration file routes.php does not exist.' );
		}

		//Prevent this from running if already set
		if( !empty( self::$options['routes'] ) )
		{
			Timer::start( 'returning existing routes' );
			return self::$options['routes'];
		}

		$routes = include( self::$core_files . 'routes.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'routes.php' ) )
		{
			$routes = array_replace_recursive( $routes, include( $file_path ) );
		}

		$routes = Router::get();

		Timer::start( 'returning fresh routes' );
		self::$options['routes'] = $routes;
		return $routes;
	}
	//end get_routes()


	/**
	 * @param string $return_var
	 * @return array|mixed
	 * @throws \Exception
	 */
	public static function get_mail( $return_var = '' )
	{
		Options::init();

		if( !file_exists( self::$core_files . 'mail.php' ) )
		{
			throw new \Exception( 'The configuration file mail.php does not exist.' );
		}

		//Prevent this from running if already set
		if( !empty( self::$options['mail'] ) )
		{
			if( !empty( $return_var ) && isset( self::$options['mail'][$return_var] ) )
			{
				return self::$options['mail'][$return_var];
			}
			return self::$options['mail'];
		}

		$mail = include( self::$core_files . 'mail.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'mail.php' ) )
		{
			$mail = array_replace_recursive( $mail, include( $file_path ) );
		}

		self::$options['mail'] = $mail;

		//Check to see if we've requested a specific variable, and if it exists return it
		if( !empty( $return_var ) && isset( $mail[$return_var] ) )
		{
			return $mail[$return_var];
		}

		return $mail;
	}
	//end get_mail()

	/**
	 * Model preparation script
	 * This only runs once and scans the views directories recursively for any PHP file
	 * @access private
	 * @param $dirname
	 * @return array
	 */
	private static function wg_glob_recursive( $dirname = VIEWS )
	{
		Timer::start( 'wg_glob_recursive initialized' );
		$files = glob( $dirname . '*', GLOB_NOSORT );

		//Remove system files
		$files = array_diff( $files, array( '..', '.' ) );

		//If the array isn't empty, loop it
		if( !empty( $files ) )
		{
			foreach( $files as $f )
			{
				//If it isn't a directory, add to the list
				if( !is_dir( $f ) )
				{
					self::$options['views'][basename( $f, '.php' )] = $f;
				}
				//Otherwise reloop
				else
				{
					self::wg_glob_recursive( realpath( $f ) . SEP );
				}
			}
		}
		Timer::start( 'wg_glob_recursive returning views' );
		return array_unique( self::$options['views'] );
	}
	//end glob_recur()


	private static function retrieve_filelist( $dirname )
	{

		$directory = new \RecursiveDirectoryIterator( $dirname, \FilesystemIterator::SKIP_DOTS );
		$iterator = new \RecursiveIteratorIterator( $directory, \RecursiveIteratorIterator::CHILD_FIRST );
		$regex = new \RegexIterator( $iterator, '/.(php)$/' );
		$objects = array();
		foreach( $regex as $filename => $current )
		{
			$objects[$current->getFileName()] = $filename;
		}
		return $objects;
	}

	public static function get_views( $view = '' )
	{
		Timer::start( 'Getting views' );
		Options::init();

		//Prevent this from running if already set and we aren't looking for a specific one
		if( !empty( self::$options['views'] ) )
		{
			Timer::start( 'Returning views already existing' );
			if( !empty( $view ) && isset( self::$options['views'][$view] ) )
			{
				return self::$options['views'][$view];
			}
			return self::$options['views'];
		}

		$views = self::wg_glob_recursive();

		self::$options['views'] = $views;

		//Check to see if we've requested a specific variable, and if it exists return it
		if( !empty( $view ) && isset( $views[$view] ) )
		{
			Timer::start( 'Returning single view' );
			return $views[$view];
		}

		Timer::start( 'Returning views that did not exist' );
		return $views;
	}
	//end get_views()


	public static function get_controllers()
	{
		Timer::start( 'Getting controllers' );
		Options::init();

		$controllers = array();

		//Prevent this from running if already set and we aren't looking for a specific one
		if( !empty( self::$options['controllers'] ) )
		{
			Timer::start( 'Returning views already existing' );
			$controllers = self::$options['controllers'];
		}
		else
		{
			$controllers = self::retrieve_filelist( CONTROLLERS );
			self::$options['controllers'] = $controllers;
			Timer::start( 'Returning controllers that did not exist' );
		}

		foreach( $controllers as $k => $v )
		{
			if( file_exists( $v ) )
			{
				require_once( $v );
			}
		} //end foreach

		return self::$options['controllers'];

	}
	//end get_controllers()


	public static function get_filesystem()
	{
		Options::init();

		if( !file_exists( self::$core_files . 'files.php' ) )
		{
			throw new \Exception( 'The configuration file files.php does not exist.' );
		}

		//Prevent this from running if already set
		if( !empty( self::$options['filesystems'] ) )
		{
			return self::$options['filesystems'];
		}

		$filesystem = include( self::$core_files . 'files.php' );

		//Check for any environmental configuration
		if( file_exists( $file_path = self::$subfolder . 'files.php' ) )
		{
			$filesystem = array_replace_recursive( $filesystem, include( $file_path ) );
		}

		$type = $filesystem['storage'];
		$filesystem = $filesystem['discs'][$type];

		self::$options['filesystems'] = $filesystem;
		return $filesystem;
	}
	//end get_filesystem()

} //end Options

/* End of file Options.php */
/* Location: /application/core/Options.class.php */