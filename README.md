RapidPHPMe
===============

A super slim PHP framework for rapid development.


#Changelog

- 27-Feb-2017: Add Models.php based caching interface to enable whole method output caching.  Added RequestType methods and static variables
- 24-Feb-2017: Update Encrypt class to use adapted version of illuminate/encryption
- 24-Feb-2017: Update ComposerInstaller to leverage .env vs. env.php files, add replacement variables for installs
- 24-Feb-2017: Autoload data-helper.php in composer.json, move functions to data-helper including .env related to use in config files
- 23-Feb-2017: Updated Sessions class to use database sessions vs. file sessions depending on config.php "session_use_db" variable
- 17-Feb-2017: Updated to rapidPHPCore V2 to accommodate different template directories, updated PHPFastCache, and more
- 25-Dec-2016: Updated html-helper docblocks.  Updated html-helper functions for get, post, session, cookie retrieval to include the ability to pass default values.  Added getParameter() function to html-helper to retrieve only $_GET or $_POST vals
- 15-Nov-2016: Updated ConfigLoader to have time constants, and make sure whoops exists prior to failure
- 01-Nov-2016: Updated EventLogger to accept an override param which disables check to see if record exists, updated template files to initialize parent constructor when extending Control
- 12-Oct-2016: Updated Paginate::getUIPagination to allow output with limited number of page links
- 1.1.1, 31-Aug-2016: Added ability to use closures in add_output method
- 1.1.1, 04-Jul-2016: Updated Dotenv env. handling
- 1.1, 30-Jun-2016: Added Dotenv for environmental variable usage for security